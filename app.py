from flask import Flask, jsonify, request
from dotenv import load_dotenv

import mariadb
import sys
import os

import random

import json

DB_LOCATION = os.getenv('DB_LOCATION')
if not DB_LOCATION:
    DB_LOCATION = os.environ.get('DB_LOCATION')

DB_USER = os.getenv('DB_USER')
if not DB_USER:
    DB_USER = os.environ.get('DB_USER')

DB_PASS = os.getenv('DB_PASS')
if not DB_PASS:
    DB_PASS = os.environ.get('DB_PASS')

DB_NAME = os.getenv('DB_NAME')
if not DB_NAME:
    DB_NAME = os.environ.get('DB_NAME')


GET_STATEMENT = "SELECT user_id, user_email, confirm_code, user_confirmed FROM reg_table WHERE user_id={0}"
INSERT_STATEMENT = "INSERT INTO reg-table (user_id,user_email,confirm_code) VALUES (?, ?, ?)"
UPDATE_STATEMENT = 'UPDATE reg_table SET user_email={1}, confirm_code={2}, user_confirmed="N" WHERE user_id=?'
GET_ALL_USERS_STATEMENT = "SELECT user_id, user_email FROM reg_table WHERE user_id!=''"
GET_USER_CONFIRM_CODE =  "SELECT confirm_code from reg_table WHERE user_id={0}"
UPDATE_USER_CONFIRMED = 'UPDATE reg_table SET user_confirmed="Y" WHERE user_id={0}'

app = Flask(__name__)

def fetch_db_connection():
    conn = mariadb.connect(
        user=DB_USER,
        password=DB_PASS,
        host=DB_LOCATION,
        port=3306,
        database=DB_NAME
    )
    return conn

def generate_confirm_code():
    return str(random.randrange(10000000,99999999))

@app.route('/')
def index():
    return 'Hello, welcome to a simple recon service'

@app.route('/users', methods=['GET'])
def get_all_users():
    conn = fetch_db_connection()
    cur = conn.cursor()
    cur.execute(GET_ALL_USERS_STATEMENT)
    for user_id, user_email in cur:
        print (f"{user_id}, {user_email}")
    conn.close()
    return 'all users printed to logs'

@app.route('/user/<int:user_id>', methods=['GET'])
def get_user(user_id):
    conn = fetch_db_connection()
    cur = conn.cursor()
    cur.execute(GET_STATEMENT.format(user_id))
    row_headers=[x[0] for x in cur.description] #this will extract row headers
    rv = cur.fetchall()
    json_data=[]
    for result in rv:
        json_data.append(dict(zip(row_headers,result)))
    json_response = {"result" : json_data}
    return jsonify(json_response), 200

@app.route('/user/<int:user_id>', methods=['PUT'])
def insert_user(user_id):
    user_email = str(request.data.decode('utf-8'))
    print("Found following string in service email parsing: " + user_email)
    try:
        conn = fetch_db_connection()
    except mariadb.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        return str(e), 500
    cur = conn.cursor()
    try :
        cur.execute("INSERT INTO reg_table (user_id,user_email,confirm_code) VALUES (?, ?, ?)", (f'{user_id}', user_email, generate_confirm_code()))
        # TODO: send the e-mail to the e-mail provided with the confirmation code
        conn.commit()
    except mariadb.Error as e:
        print(f"Error inserting user to DB: {e}")
        return str(e), 500
    finally:
        conn.close()
    return '', 201

@app.route('/user/<int:user_id>/confirm/<int:confirm_code>', methods=['POST'])
def confirm_user_email(user_id, confirm_code):
    conn = fetch_db_connection()
    cur = conn.cursor()
    cur.execute(GET_USER_CONFIRM_CODE.format(user_id))
    confirm_codes = []
    for confirm_code in cur:
        confirm_codes.append(confirm_code)
        print(f'found confirm code {confirm_code} for user {user_id}')
    if confirm_code in confirm_codes:
        cur.execute(UPDATE_USER_CONFIRMED.format(user_id))
        conn.commit()
        conn.close()
        return '', 200
    return 'Invalid confirm code', 400

@app.route('/user/<int:user_id>', methods=['POST'])
def update_user(user_id):
    return f'we are doing some logic to update user : {user_id}'

if __name__ == '__main__':
    app.run(debug=False)
